from MimV6 import statistics, PacketStats
from scapy.all import Ether, IP, TCP, UDP, IPv6

statistics.append(PacketStats(Ether()))
statistics.append(PacketStats(Ether()/IP()))
statistics.append(PacketStats(Ether()/IP()/TCP()))
statistics.append(PacketStats(Ether()/IP()/UDP()))
statistics.append(PacketStats(Ether()/IPv6()/TCP()))
statistics.append(PacketStats(Ether()/IPv6()/UDP()))
statistics.append(PacketStats(Ether()/IPv6()))
statistics.append(PacketStats(Ether()))

print(f"""
UDP: {PacketStats.udp}
TCP: {PacketStats.tcp}
Other: {PacketStats.other}
""")

print("Lenght: " + ", ".join([str(pkt.lenght) for pkt in statistics]))