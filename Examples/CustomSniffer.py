from MimV6 import Sniffer, Spoofer, PacketPrinter

class CustomAnalysis (Sniffer):
	
	def __init__ (self):
		super().__init__("all", PacketPrinter(), None)

	def analysis (self, packet):
		print(bytes(packet))

attack = Spoofer("all", CustomAnalysis())
attack.launcher()